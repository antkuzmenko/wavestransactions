package com.itrex.wavestransactions;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.PrivateKey;

import com.wavesplatform.wavesj.Node;
import com.wavesplatform.wavesj.PrivateKeyAccount;
import com.wavesplatform.wavesj.PublicKeyAccount;
import com.wavesplatform.wavesj.Transaction;

public class AddNewAsset {

    static String seed = "asdfg";
    static PrivateKeyAccount accountPrivateKey = PrivateKeyAccount.fromSeed(seed, 0, (byte) 'K');
    static byte[] publicKey = accountPrivateKey.getPublicKey();
    static PublicKeyAccount accountPublicKey = new PublicKeyAccount(publicKey, (byte) 'K');
    static String address = accountPrivateKey.getAddress();

    public static void main(String[] args) throws URISyntaxException, IOException {

        Node node = new Node("http://127.0.0.1:8420");
        System.out.println("Current height is " + node.getHeight());
        System.out.println("My balance is " + node.getBalance(address));
        System.out.println("With 100 confirmations: " + node.getBalance(address, 100));

        Transaction newAsset = Transaction.makeIssueTx(accountPublicKey, (byte) 'K', "EuroJOOLS", "Custom EuroJOOLS asset", 1000000000000000000L, (byte) 10, true, null,0L);
        System.out.println("JSON encoded data: " + newAsset.getJson());
        System.out.println("Server endpoint to send this JSON to: " + newAsset.toString());

        //        Now send it from an online machine:
        //2917 block

        node.send(newAsset);

    }
}
