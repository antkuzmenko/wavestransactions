package com.itrex.wavestransactions;

import java.io.IOException;
import java.net.URISyntaxException;

import com.wavesplatform.wavesj.Account;
import com.wavesplatform.wavesj.Asset;
import com.wavesplatform.wavesj.Node;
import com.wavesplatform.wavesj.PrivateKeyAccount;
import com.wavesplatform.wavesj.Transaction;

public class TransactionApp {

    static String seed = "asdfg";
    static PrivateKeyAccount account = PrivateKeyAccount.fromSeed(seed, 0, (byte) 'K');
    static byte[] publicKey = account.getPublicKey();
    static String address = account.getAddress();

    public static void main(String[] args) throws URISyntaxException, IOException {

        //        Create a Node and learn a few things about blockchain:

        Node node = new Node("http://127.0.0.1:8420");
        System.out.println("Current height is " + node.getHeight());
        System.out.println("My balance is " + node.getBalance(address));
        System.out.println("With 100 confirmations: " + node.getBalance(address, 100));

        //        Send some money to a buddy:
//        for (int i = 0; i < 5; i++) {
            String buddy = "3JGtQ3uhAYnSjka3NQmBYK1WS1mSJjWSQum";
//            String txId = node.transfer(account, buddy, 1_00000000, 100_000, "Here's for you");

            //        Sign a transaction.js offline:

//            Transaction tx = Transaction.makeTransferTx(account, buddy, 10000000000L,Asset.WAVES, 0, Asset.WAVES, "");
            Transaction tx = Transaction.makeTransferTx(account, buddy, 10000000000L,"2amwjFWo34W4tzJ76VBSRh3niAdwEXBKXkfq7vLWYAyF", 0, "2amwjFWo34W4tzJ76VBSRh3niAdwEXBKXkfq7vLWYAyF", "");
            System.out.println("JSON encoded data: " + tx.getJson());
            System.out.println("Server endpoint to send this JSON to: " + tx.toString());

            //        Now send it from an online machine:

            node.send(tx);


//        }
    }

}
